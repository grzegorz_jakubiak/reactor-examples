package com.example.reactorexamples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class ReactorExamplesApplication {

    public static void main(String[] args)
    {
        SpringApplication.run(ReactorExamplesApplication.class, args);

        Flux<String> fruitFlux = Flux.just("apple", "orange", "grape");
        fruitFlux.subscribe(o -> System.out.println(o));
    }



}
